# Master Utility Package

## **Support**

    - Android
    - iOS

### How to add this package to your project add this dependencies inside your pubspec.yaml file

    dependencies:
       master_utility :
          git:
            url: "https://gitlab.com/inamh_webelight/master_utility.git"
            ref: dev

## Import Package

> import 'package:master_utility/master_utility.dart';

### This package contains the below points as of now. We can improve and add new points as per requirements in the future

## **Navigation helper**

        NavigationHelper.navigatePop();

        NavigationHelper.navigatePush(route: routeName());

        NavigationHelper.navigatePushReplacement(route: routeName());

        NavigationHelper.navigatePushRemoveUntil(route: routeName());

## **Sizer helper**

> add below line inside your first widget build method

        SizeHelper.setMediaQuerySize();

        Container(
           width: 0.03.w,    //It will take a 20% of screen width
           height:0.03.h,     //It will take a 30% of screen height
        ),

        Padding(
           padding: EdgeInsets.symmetric(vertical: 0.05.h, horizontal: 0.05.w),
           child: Container(),
        );

        Text(
           'Sizer Helper',style: TextStyle(fontSize: 5.5.fs),
        );

## **Image picker helper**

  > To use In [IOS] add this code in project_directory/ios/Runner/info.plist

        <key>NSCameraUsageDescription</key>
        <string>Upload image from camera for screen background</string>
        <key>NSMicrophoneUsageDescription</key>
        <string>Post videos to profile</string>
        <key>NSPhotoLibraryUsageDescription</key>
        <string>Upload images for screen background</string>

- Multimedia picker
  - Take Photo
  - Photo from Gallery
  - Take a Video
  - Video from Gallery
  - Document

        final result = await ImagePickerHelper.multiMediaPicker();

- Custom media picker

        final result =  await ImagePickerHelper.customMediaPicker( pickerActionType: PickerActionType.camera);
        use below enum for custom media picker
        PickerActionType.camera
        PickerActionType.gallery,
        PickerActionType.video,
        PickerActionType.cameraVideo,
        PickerActionType.document,

- Date time helper
- Auto size text helper
- Toast helper
- Log helper
- Dialog Helper
- Cache network Image Helper
- Custom internet connectivity check
- Validation helper
- Error Helper
- Api Helper
- Shared Preference Helper

## Permission in Flutter App

> Open the AndroidManifest.xml file located at ./android/app/src/main and add the following line

    <uses-permission android:name="android.permission.INTERNET"/>

    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    
    <uses-permission android:name="android.permission.ACCESS_LOCATION_EXTRA_COMMANDS" />
    
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />

    <uses-permission android:name="com.example.towntour.permission.MAPS_RECEIVE" />

    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

    <uses-permission android:name="android.permission.CALL_PHONE" />
    
    <uses-permission android:name="android.permission.READ_PHONE_STATE" />   

    <uses-permission android:name="com.android.vending.BILLING" />

    <uses-permission android:name="android.permission.WRITE_INTERNAL_STORAGE" />

    <uses-permission android:name="android.permission.READ_INTERNAL_STORAGE" />

    <uses-feature android:name="android.hardware.camera" />

    <uses-permission android:name="android.permission.MANAGE_EXTERNAL_STORAGE" />

    <uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS" />

    <uses-permission android:name="android.permission.RECORD_AUDIO" />

    <uses-permission android:name="android.permission.BLUETOOTH" />

    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>

    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>

    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED"/>

    <uses-permission android:name="android.permission.FOREGROUND_SERVICE" />

    <uses-permission android:name="android.permission.ACCESS_NOTIFICATION_POLICY" />

    <uses-permission android:name="android.permission.WAKE_LOCK"/>

    <uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />

    <uses-permission android:name="android.permission.CHANGE_NETWORK_STATE" />

    <uses-permission android:name="android.permission.READ_PHONE_STATE" />

    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />

## How to install pods in Flutter App

- Now open the project in Vs Code or Android Studio.In the terminal, go to the ios directory using cd ios. And run the commands below step by step.

       sudo arch -x86_64 gem install ffi

    ( when you run this command it will ask for a password. You need enter your system password )

        arch -x86_64 pod install
        arch -x86_64 pod update
        pod setup

- How to open an app in Xcode
  - There are two ways.

    1) using command

        - Open Runner.xcworkspace ( inside in your ios directory )

    2) on your project directory

        - right click on ios folder and select " Open in Xcode ".

## Firebase Message Service

> add this file in your project

[firebase_messaging_service](https://gitlab.com/inamh_webelight/master_utility/-/wikis/uploads/8de8bbdf24fb65d9d29aea0728e580c5/firebase_messaging_service__1_.dart)

## Local Notification Service

> add this file in your project

[local_notification_service](https://gitlab.com/inamh_webelight/master_utility/-/wikis/uploads/cad9b91866cae22e9efd81ed7992c43c/local_notification_service__1_.dart)
