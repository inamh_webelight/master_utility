import 'dart:developer';

import 'package:flutter/foundation.dart';

class Log {
  /// For disable log | For set variable use [setIsLogVisible] method in `` void main(){} ``
  /// ```
  /// bool isLogVisible = true;
  /// ```
  static bool _isLogVisible = true;
  bool get isLogVisible => _isLogVisible;
  static void setIsLogVisible({required bool isLogVisible}) {
    _isLogVisible = isLogVisible;
  }

  /// [logInfo] print Blue Color
  static void logInfo(String msg) {
    // Blue text
    kDebugMode
        ? _isLogVisible
            ? log('\x1B[94m$msg\x1B[0m')
            : null
        : null;
  }

  /// [logSuccess] print Green Color
  static void logSuccess(String msg) {
    // Green text
    kDebugMode
        ? _isLogVisible
            ? log('\x1B[92m$msg\x1B[0m')
            : null
        : null;
  }

  /// [logWarning] print Yellow Color
  static void logWarning(String msg) {
    // Yellow text
    kDebugMode
        ? _isLogVisible
            ? log('\x1B[33m$msg\x1B[0m')
            : null
        : null;
  }

  /// [logError] print Red Color
  static void logError(String msg) {
    // Red text
    kDebugMode
        ? _isLogVisible
            ? log('\x1B[91m$msg\x1B[0m')
            : null
        : null;
  }

  /// [logCyan] print Cyan Color
  static void logCyan(String msg) {
    // Cyan text
    kDebugMode
        ? _isLogVisible
            ? log('\x1B[36m$msg\x1B[0m')
            : null
        : null;
  }
}
