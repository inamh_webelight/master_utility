import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:master_utility/master_utility.dart';

class CustomError extends StatelessWidget {
  final FlutterErrorDetails errorDetails;

  const CustomError({
    Key? key,
    required this.errorDetails,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeHelper.setMediaQuerySize();
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 0.1.w),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ConstrainedBox(
              constraints: const BoxConstraints(maxHeight: 200, maxWidth: 300),
              child: AppNetworkImage(
                  height: 0.3.h,
                  width: 0.3.h,
                  fit: BoxFit.cover,
                  url:
                      "https://i.postimg.cc/Hnz5qx5N/404-error-with-person-looking-for-bro-c-min.png"),
            ),
            const SizedBox(height: 20),
            AutoText(
              text: kDebugMode
                  ? errorDetails.summary.toString()
                  : 'Oups! Something went wrong!',
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: kDebugMode
                      ? Colors.red
                      : Theme.of(context).textTheme.titleMedium?.color ??
                          Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 0.6.fs),
              maxFontSize: 30,
            ),
            const SizedBox(height: 12),
            AutoText(
              text: kDebugMode
                  ? 'https://docs.flutter.dev/testing/errors'
                  : "We encountered an error and we've notified our engineering team about it. Sorry for the inconvenience caused.",
              textAlign: TextAlign.center,
              maxLines: 3,
              maxFontSize: 25,
              style: TextStyle(
                  color: Theme.of(context).textTheme.subtitle1?.color ??
                      Colors.grey,
                  fontSize: 0.5.fs,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1),
            ),
          ],
        ),
      ),
    );
  }
}
