import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenceServiceHelper {
  static SharedPreferences? _prefs;
  static Future<SharedPreferences?> getPrefObject() async {
    _prefs ?? await SharedPreferences.getInstance();
    return _prefs;
  }

  Future<void> setBoolPrefValue({
    required String key,
    required bool value,
  }) async {
    _prefs = await getPrefObject();
    _prefs?.setBool(key, value);
  }

  Future<bool> getBoolPrefValue({required String key}) async {
    _prefs = await getPrefObject();

    return _prefs?.getBool(key) ?? false;
  }

  Future<void> setStringPrefValue({
    required String key,
    required String value,
  }) async {
    _prefs = await getPrefObject();

    _prefs?.setString(key, value);
  }

  Future<String> getStringPrefValue({required String key}) async {
    _prefs = await getPrefObject();

    return _prefs?.getString(key) ?? '';
  }

  Future<void> setMapPrefValue({
    required String key,
    required Map<String, dynamic> value,
  }) async {
    _prefs = await getPrefObject();

    final String encodedString = json.encode(value);
    _prefs?.setString(key, encodedString);
  }

  Future<Map<String, dynamic>?> getMapPrefValue({
    required String key,
  }) async {
    _prefs = await getPrefObject();

    final String decodedString = _prefs?.getString(key) ?? '';
    return decodedString.isEmpty ? null : json.decode(decodedString);
  }

  Future<void> setIntPrefValue({
    required String key,
    required int value,
  }) async {
    _prefs = await getPrefObject();

    _prefs?.setInt(key, value);
  }

  Future<int> getIntPrefValue({
    required String key,
  }) async {
    _prefs = await getPrefObject();

    return _prefs?.getInt(key) ?? 0;
  }

  Future<bool> clearAll() async {
    _prefs = await getPrefObject();

    final Future<bool> value = _prefs!.clear();
    return value;
  }
}
