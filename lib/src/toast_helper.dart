import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:master_utility/master_utility.dart';

class ToastHelper {
  static void showToast({required String message}) {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: Theme.of(NavigationService.navigatorKey.currentContext!)
          .backgroundColor,
      textColor: Colors.white,
      fontSize: 0.5.fs,
    );
  }

  static void showCustomToast({
    required String message,
    Color? backgroundColor,
    Color? textColor,
    double? fontSize,
    ToastGravity? gravity,
  }) {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: gravity ?? ToastGravity.BOTTOM,
      backgroundColor: backgroundColor ?? Colors.black.withOpacity(0.7),
      textColor: textColor ?? Colors.white,
      fontSize: fontSize ?? 0.5.fs,
    );
  }
}
