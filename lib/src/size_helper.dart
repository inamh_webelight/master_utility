import 'package:flutter/widgets.dart';
import 'package:master_utility/master_utility.dart';

double _height = 0.0;
double _width = 0.0;
double _fontSize = 0.0;

class SizeHelper {
  static setMediaQuerySize() {
    _height = MediaQuery.of(NavigationService.navigatorKey.currentContext!)
        .size
        .height;
    _width = MediaQuery.of(NavigationService.navigatorKey.currentContext!)
        .size
        .width;
    _fontSize = _height * _width * 0.01 / 100;
  }
}

extension SizerDouble on num {
  double get h => _height * this;
  double get w => _width * this;
  double get fs => _fontSize * this;
}
