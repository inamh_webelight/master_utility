import 'package:flutter/material.dart';
import 'package:master_utility/src/material_widget.dart';

class NavigationHelper {
  static final BuildContext _context =
      NavigationService.navigatorKey.currentContext!;

  static navigatePush({required Widget route}) {
    return Navigator.push(
        _context,
        MaterialPageRoute(
          builder: (context) => route,
        ));
  }

  static navigatePop() {
    Navigator.pop(_context);
  }

  // ignore: non_constant_identifier_names
  static Future navigatePushReplacement({route}) {
    return Navigator.pushReplacement(
        _context, MaterialPageRoute(builder: (context) => route));
  }

  static Future navigatePushRemoveUntil({route}) {
    return Navigator.pushAndRemoveUntil(_context,
        MaterialPageRoute(builder: (context) => route), (route) => true);
  }
}
