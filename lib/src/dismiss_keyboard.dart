import 'package:flutter/material.dart';
import 'package:master_utility/src/log_helper.dart';

class DismissKeyboardOnScroll extends StatelessWidget {
  const DismissKeyboardOnScroll({Key? key, required this.child, this.onDismiss})
      : super(key: key);

  final Widget child;
  final Function()? onDismiss;

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollStartNotification>(
      onNotification: (ScrollStartNotification notification) {
        if (notification.dragDetails == null) {
          return false;
        }
        if (onDismiss != null) {
          onDismiss?.call();
        }
        final FocusScopeNode currentScope = FocusScope.of(context);
        if (!currentScope.hasPrimaryFocus && currentScope.hasFocus) {
          FocusManager.instance.primaryFocus!.unfocus();
        }
        return true;
      },
      child: child,
    );
  }
}

class DismissKeyboardOnTap extends StatelessWidget {
  const DismissKeyboardOnTap({Key? key, required this.child, this.onDismiss})
      : super(key: key);

  final Widget child;
  final Function()? onDismiss;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Log.logWarning('GestureDetector tapped...');
        if (onDismiss != null) {
          onDismiss?.call();
        }
        final FocusScopeNode currentScope = FocusScope.of(context);
        if (!currentScope.hasPrimaryFocus && currentScope.hasFocus) {
          FocusManager.instance.primaryFocus!.unfocus();
        }
      },
      child: child,
    );
  }
}
