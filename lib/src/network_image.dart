import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppNetworkImage extends StatelessWidget {
  final String url;
  final BoxFit? fit;
  final double? height;
  final double? width;

  const AppNetworkImage({
    Key? key,
    this.fit,
    this.height,
    this.width,
    required this.url,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: url,
      height: height,
      width: width,
      progressIndicatorBuilder: (_, __, ___) => _buildProgressIndicator(),
      fit: fit ?? BoxFit.cover,
      errorWidget: (context, url, error) => _onError(),
    );
  }

  Widget _buildProgressIndicator() {
    return const CupertinoActivityIndicator();
  }

  Widget _onError() {
    return const Icon(
      Icons.error,
      color: Colors.red,
    );
  }
}
