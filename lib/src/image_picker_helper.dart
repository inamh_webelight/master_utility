import 'dart:io';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:image_picker/image_picker.dart';
import 'package:file_picker/file_picker.dart';

import 'package:master_utility/src/dialog_helper.dart';
import 'package:master_utility/src/log_helper.dart';

enum PickerActionType {
  camera,
  gallery,
  video,
  cameraVideo,
  document,
}

/// To use In [IOS] add this code in `` project_directory/ios/Runner/info.plist ``
///
///``use``
///```
///final result = await ImagePickerHelper.openImagePicker();
///```
/// ```
/// <key>NSCameraUsageDescription</key>
/// <string>Upload image from camera for screen background</string>
///	<key>NSMicrophoneUsageDescription</key>
///	<string>Post videos to profile</string>
///	<key>NSPhotoLibraryUsageDescription</key>
///	<string>Upload images for screen background</string>
/// ```
class ImagePickerHelper {
  static Future<File?> multiMediaPicker() async {
    String? pickedFilePath;
    final PickerActionType? actionType =
        await DialogHelper.showActionSheet<PickerActionType>(
      actions: const [
        SheetAction(
          key: PickerActionType.camera,
          label: 'Take Photo',
        ),
        SheetAction(
          key: PickerActionType.gallery,
          label: 'Photo from Gallery',
        ),
        SheetAction(
          key: PickerActionType.cameraVideo,
          label: 'Take a Video',
        ),
        SheetAction(
          key: PickerActionType.video,
          label: 'Video from Gallery',
        ),
        SheetAction(
          key: PickerActionType.document,
          label: 'Document',
        ),
      ],
    );
    await _imagePickerSwitch(
        pickedFilePath: pickedFilePath, actionType: actionType);

    Log.logSuccess(" File path is : $pickedFilePath");
    return pickedFilePath != null ? File(pickedFilePath) : null;
  }

  static Future<File?> customMediaPicker({
    required PickerActionType pickerActionType,
  }) async {
    String? pickedFilePath;

    _imagePickerSwitch(
        actionType: pickerActionType, pickedFilePath: pickedFilePath);
    Log.logSuccess(" File path is : $pickedFilePath");
    return pickedFilePath != null ? File(pickedFilePath) : null;
  }

  static _imagePickerSwitch({pickedFilePath, actionType}) async {
    switch (actionType) {
      case PickerActionType.camera:
        final ImagePicker picker = ImagePicker();
        final XFile? photo = await picker.pickImage(source: ImageSource.camera);
        pickedFilePath = photo?.path;
        break;
      case PickerActionType.gallery:
        final ImagePicker picker = ImagePicker();
        final XFile? image =
            await picker.pickImage(source: ImageSource.gallery);
        pickedFilePath = image?.path;
        break;
      case PickerActionType.video:
        final ImagePicker picker = ImagePicker();
        final XFile? video =
            await picker.pickVideo(source: ImageSource.gallery);
        pickedFilePath = video?.path;
        break;
      case PickerActionType.cameraVideo:
        final ImagePicker picker = ImagePicker();
        final XFile? video = await picker.pickVideo(source: ImageSource.camera);
        pickedFilePath = video?.path;
        break;
      case PickerActionType.document:
        final picker = await FilePicker.platform.pickFiles(
          allowMultiple: false,
          allowedExtensions: ["pdf", "xlsx"],
          type: FileType.custom,
        );
        final XFile file = XFile(picker?.files.single.path ?? "");
        pickedFilePath = file.path;
        break;
      default:
        return null;
    }
  }
}
