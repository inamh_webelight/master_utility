part of 'api_service.dart';

class APIResponse<T> {
  APIResponse({
    this.hasError,
    this.message,
    this.statusCode,
    this.data,
  });

  APIResponse.fromJson(
    dynamic json, {
    Function(T)? create,
  }) {
    hasError =
        (json is List<dynamic>) ? null : (json?['error'])?.isNotEmpty ?? false;
    message = (json is List<dynamic>) ? null : json?['message'] ?? '';
    statusCode = (json is List<dynamic>) ? null : json?['status'] ?? 0;
    data = (json != null && create != null)
        ? create(
            json as T,
          )
        : null;
  }

  factory APIResponse.custom({required String message}) {
    return APIResponse<T>(message: message, hasError: true);
  }

  bool? hasError;
  String? message;
  int? statusCode;
  dynamic data;
}
