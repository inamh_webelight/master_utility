import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:master_utility/master_utility.dart';

class InternetConnectivityHelper {
  static Future<bool> checkInternet() async {
    try {
      final ConnectivityResult connectivityResult =
          await Connectivity().checkConnectivity();
      final bool result = connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi;
      if (!result) {
        DialogHelper.showOkDialog(
          message: "You need to have Mobile Data or wifi to access this.",
          title: "No Internet Connection",
        );
      }
      return result;
    } on SocketException catch (_) {
      return false;
    }
  }
}
